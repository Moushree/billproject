<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
class Admincon extends Controller
{
    
    public function __construct()
    {
        // Closure as callback
       
           
            $this->middleware(function ($request, $next) {
                $u=Auth::user();
               if($u->role!='admin')
               {
                   return redirect('/admin-login');
               }
    
                return $next($request);
            });
    }
    public function admin(Request $r)
    {
        // $u=Auth::user();
        // if($u->role!='admin')
        // {
        //     return redirect('/admin-login');
        // }
      
        return view('admin');
    }
    public function empform(){
        return view ('addemp');
    }
    public function addemployee(Request $r)
    {
        $en=$r->name;
        $ep=$r->password;
        $ee=$r->email;
        $eph=$r->phone;
        $ea=$r->address;


        $obj = new User();
        $obj->name=$en;
        $obj->email=$ee;
        $obj->password=bcrypt($ep);
        $obj->phone=$eph;
        $obj->role="employee";
        $obj->address=$ea;
        $obj->save();

        $r->session()->flash("msg","Employee Inserted Succesfully");
        return redirect('employee-form');
    }
    
}
