<?php

namespace App\Http\Controllers;

use App\Models\Billmod;
use Illuminate\Http\Request;
use App\Models\CustMod;
use App\Models\BillConfirmMod;
use App\Models\Promod;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use PhpParser\Node\Stmt\Foreach_;

class Billcontroller extends Controller
{
    public function addbill(Request $r)
    {
        $id=$r->id;
        $obj = CustMod::find($id);
        $w=array(
            'r' => $obj,
        );
        return view('addbill')->with($w);
    }
    public function addbillins(Request $r)
    {
        $validator = Validator::make($r->all(), [
            'barcode' => 'required'
        ]);

        if($validator->fails())
        {
            return response()->json([
                'status'=>400,
                'errors'=>$validator->errors()
            ]);
        }
        else
        {
            $barcode = $r->input('barcode');
            $check_barcode = Billmod::where('barcode','=',$barcode)->get();
            if(count($check_barcode)>0)
            {
                return response()->json([
                    'status'=>400,
                    'errors'=>array(
                       'barcode'=>"Barcode already exsist"
                    )
                ]);
            }else{

            $obj = new Billmod();
            $obj->cid = $r->input('custid');
            $obj->barcode= $r->input('barcode');
            $obj->save();
            return response()->json([
                'status' => 200,
                'message' => 'product added successfully'
            ]);
        }
        }
    }
    public function viewbillproduct()
    {
      
        $product = DB::table('temporary_table')->leftjoin('product','temporary_table.barcode','=','product.pbarcode')->select('temporary_table.*','product.pname','product.pprice','product.pbarcode')->get();

       return response()->json([
            'product' => $product
       ]);
    }
    public function deletebillproduct(Request $r)
    {
        $id=$r->id;
        $obj=Billmod::find($id);
        $obj->delete();
        $product = DB::table('temporary_table')->leftjoin('product','temporary_table.barcode','=','product.pbarcode')->select('temporary_table.*','product.pname','product.pprice','product.pbarcode')->get();

        return response()->json([
             'product' => $product
        ]);
    }
    public function confirmOrder(Request $r)
    {
       $cid=$r->cid;
       $obj=BillMod::where('cid',"=",$cid)->get();
       foreach($obj as $data)
       {
           $cid = $data->cid;
           $barcode = $data->barcode;
           $bill = new BillConfirmMod();
           $bill->cid = $cid;
           $bill->barcode = $barcode;
           $bill->save();
           $product_sold = Promod::where("pbarcode","=",$barcode)->update(['is_sold'=>'yes']);

            
       }
       $xvb=BillMod::where('cid',"=",$cid)->delete();

    }

}
