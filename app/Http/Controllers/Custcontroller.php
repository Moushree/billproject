<?php

namespace App\Http\Controllers;

use App\Models\CustMod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class Custcontroller extends Controller
{
    public function addcustomer()
    {
        return view('addcustomer');
    }
    public function customerins(Request $r)
    {
        $name=$r->name;
        $email=$r->email;
        $phone=$r->phone;
        $ab=Auth::user()->id;
    


        $obj= new CustMod();
        $obj->name=$name;
        $obj->email=$email;
        $obj->phone=$phone;
        $obj->added_by=$ab;
        $obj->save();

        $r->session()->flash("msg"," Customer added Successfull");
        return redirect('customer');
      

    }
    public function viewcustomer()
    {
        $obj=CustMod::all();
        $w=array(
            'row'=>$obj
        );
        return view('viewcustomer')->with($w);
    }

}
