<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

use Illuminate\Support\Facades\Auth;

class Emplara extends Controller
{
    public function __construct()
    {
        // Closure as callback
       
           
            $this->middleware(function ($request, $next) {
                $u=Auth::user();
               if($u->role!='employee')
               {
                   return redirect('/admin-login');
               }
    
                return $next($request);
            });
    }
   
    public function viewemployee(Request $r)
    {
        $obj=User::all();
        $w=array(
            'row'=>$obj
        );
        return view('viewemployee')->with($w);
    }
   
  
 
    public function employee(Request $r)
    {
        // $u=Auth::user();
        // if($u->role!='employee')
        // {
        //     return redirect('/admin-login');
        // }
        return view('employee');
    }
    
}
