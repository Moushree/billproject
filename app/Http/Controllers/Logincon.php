<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
class Logincon extends Controller
{
    public function adminlogin()
    {
        return view("alogin");
    }
    public function role()
    {
        return redirect('/dashboard');
    

    }
    public function logincheck(Request $r)
    {
        $email=$r->email;
        $pass=$r->password;
        $w=array(
            'email'=>$email,
            'password'=>$pass,
        );
        if(Auth::attempt($w)){
            return redirect('/role');
        }
        else
        {
          
        $r->session()->flash("msg","Invalid username/password"); 
        return redirect('admin-login'); 
        }
    }

}
