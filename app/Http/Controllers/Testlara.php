<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; 
use App\Models\Promod;

class Testlara extends Controller
{
    public function dashboard()
    {
        return view("dashboard");

    }
    public function productform()
    {
        return view("productform");
    }
    public function productins(Request $r)
    {
        $pn=$r->pname;
        $ps=$r->psize;
        $pp=$r->pprice;
        $pb=$r->pbarcode;
        $fl=$r->file('proimg');
        $fn=$fl->getClientOriginalName();

        $fl->move("productimg",$fn);

        $obj= new Promod();
        $obj->pname=$pn;
        $obj->psize=$ps;
        $obj->pprice=$pp;
        $obj->pbarcode=$pb;
        $obj->pimage=$fn;
        $obj->save();

        $r->session()->flash("msg","Product Inserted Successfull");
        return redirect('product');
      

    }
    public function viewpro(Request $r)
    {
        $obj=Promod::all();
        $w=array(
            'row'=>$obj
        );
        return view('viewproduct')->with($w);
    }
    public function productdel(Request $r){
        $id=$r->id;
        $imgd=Promod::find($id);
        $img=$imgd->pimage;

        unlink('./productimg/'.$img);
        $obj=Promod::find($id);
        $obj->delete();

        $r->session()->flash("dmsg","Product Deleted");
        return redirect('viewproduct');
    }
    public function productedit(Request $r)
    {
        $id = $r->id;
        $obj= Promod::find($id);
        $w=array(
            'row'=>$obj
        );
        return view('editproduct',$w);
    }
    public function producteditins(Request $r)
    {
        $id=$r->id;
        $pn=$r->pname;
        $ps=$r->psize;
        $pp=$r->pprice;
        $pb=$r->pbarcode;
        $fl=$r->file('proimg');
        if(isset($fl)){
            $fn=$fl->getClientOriginalName();
            $fl->move("productimg",$fn);
        }
    
        $obj=  Promod::find($id);
        $obj->pname=$pn;
        $obj->psize=$ps;
        $obj->pprice=$pp;
        $obj->pbarcode=$pb;
        if(isset($fl)){
            $obj->pimage=$fn;
        }
        
        $obj->update();

        $r->session()->flash("emsg","Product Updated Successfull");
        return redirect('viewproduct'); 
    }
}
