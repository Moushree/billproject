<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>SB Admin 2 - Blank</title>

    <!-- Custom fonts for this template-->
    <link href="{{url('/admin/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
 

    <!-- Custom styles for this template-->
    <link href="{{url('/admin/css/sb-admin-2.min.css')}}" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        @include('inc.sidebar');
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                @include('inc.header');
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Add Bill</h1>
                    @if(Session::has('msg'))
                    <div class="alert alert-success alert-dismissible fade show">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <strong>{{Session::get('msg')}}</strong>
                    </div>
                    @endif

                    <h2>Customer Details</h2>
                   
                        <p>Name : {{$r->name}}</p>
                        <p>Email : {{$r->email}}</p>
                        <p>Phone Number : {{$r->phone}}</p>
      
                    <form action="" method="post" enctype="multipart/form-data">
                      @csrf
                        <input type="hidden" class="custid" value={{$r->id}} name="custid"/>
                        <div class="form-group">
                          <label for="email">Barcode:</label>
                          <input type="text" class="form-control barcode" name="barcode" >
                          <span id="barcode_error"></span>
                        </div>
                       
                         
                        
                        <button type="submit" class="btn btn-primary add_bill">Submit</button>
                      </form>

                </div>
                <!-- /.container-fluid -->
                <div class="container pt-5">
                  
                  <h3 class="text-center pb-2">Product Details</h3>            
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>Product Name</th>
                        <th>Product Barcode</th>
                        <th>Product Price</th>
                        <th>Remove</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                  <form method="POST" action="{{url('/confirmOrder')}}">
                     @csrf
                     <input type="hidden" value="{{$r->id}}" name="cid"/>
                    <input type="submit" class="btn btn-primary" value="Confirm Order" name="confirm"/> 
                  </form>
                  
                </div>

            </div>
            <!-- End of Main Content -->

           @include('inc.footer');
           <script>
              function del(id){
                  
               if(confirm('Are you sure?')){
                  $.ajax({
                    type:'GET',
                    url:'/billproject/deletebillproduct/'+id,
                    dataType:'json',
                    success:function(response)
                    {
                      console.log(response.product);
                      $('tbody').html("");
                      $.each(response.product, function (key, items){
                       
                        $('tbody').append(

                          '<tr>\
                            <td>'+items.pname+'</td>\
                            <td>'+items.pbarcode+'</td>\
                            <td>'+items.pprice+'</td>\
                            <td><a href="javascript:void(0);" onclick="del('+items.id+')" class="btn btn-danger"><i class="fa fa-times"></i></a></td>\
                          </tr>');
                      })
                    }
                  });
               }
               
                




                }

             $(document).ready(function(){

             
              fetchproduct();

                function fetchproduct()
                {
                  $.ajax({
                    type:'GET',
                    url:'/billproject/viewbillproduct',
                    dataType:'json',
                    success:function(response)
                    {
                      console.log(response.product);
                      $('tbody').html("");
                      $.each(response.product, function (key, items){
                       
                        $('tbody').append(

                          '<tr>\
                            <td>'+items.pname+'</td>\
                            <td>'+items.pbarcode+'</td>\
                            <td>'+items.pprice+'</td>\
                            <td><a href="javascript:void(0);" onclick="del('+items.id+')" class="btn btn-danger"><i class="fa fa-times"></i></a></td>\
                          </tr>');
                      })
                    }
                  });
               
                }

                


                  $(document).on('click','.add_bill',function(e){
                  e.preventDefault();
                  var data={
                    'barcode':$('.barcode').val(),
                    'custid':$('.custid').val(),
                    
                  }
                  $.ajaxSetup({
                      headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                  });
                  $.ajax({
                    type:'POST',
                    url:'/billproject/addbillins',
                    data:data,
                    dataType:'json',
                    success:function(response)
                    {
                      //console.log(response);
                      if(response.status == 400)
                      {
                          $('#barcode_erroe').html("");
                          $('#barcode_error').css('color','red')
                          $('#barcode_error').text('**'+response.errors.barcode)
                          
                      }
                      else{
                        $('#barcode_erroe').html("");
                          $('#barcode_error').css('color','red')
                          $('#barcode_error').text("")
                        $('.barcode').val("")
                        fetchproduct();
                      }
                     
                     
                    }
                  })
                });

             })
             
          </script>

</body>

</html>