<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Blank</title>

    <!-- Custom fonts for this template-->
    <link href="{{url('/admin/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
 

    <!-- Custom styles for this template-->
    <link href="{{url('/admin/css/sb-admin-2.min.css')}}" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        @include('inc.sidebar');
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                @include('inc.header');
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Add Product</h1>
                    @if(Session::has('msg'))
                    <div class="alert alert-success alert-dismissible fade show">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <strong>{{Session::get('msg')}}</strong>
                    </div>
                    @endif
                  
                    <form action="productins" method="post" enctype="multipart/form-data">
                      @csrf
                        <div class="form-group">
                          <label for="email">Product Name:</label>
                          <input type="text" class="form-control" name="pname"  id="email">
                        </div>
                        <div class="form-group">
                            <label for="email">Product Size:</label>
                            <input type="text" class="form-control" name="psize"  id="email">
                          </div>
                          <div class="form-group">
                            <label for="email">Product Price:</label>
                            <input type="text" class="form-control" name="pprice"  id="email">
                          </div>
                          <div class="form-group">
                            <label for="email">Product Barcode:</label>
                            <input type="text" class="form-control" name="pbarcode"  id="email">
                          </div>
                          <div class="form-group">
                            <label for="email">Product Image:</label>
                            <input type="file" name="proimg">
                          </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </form>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

           @include('inc.footer');

</body>

</html>