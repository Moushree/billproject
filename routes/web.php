<?php

use App\Http\Controllers\Custcontroller;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Testlara;
use App\Http\Controllers\Emplara;
use App\Http\Controllers\Admincon;
use App\Http\Controllers\Logincon;
use App\Http\Controllers\Billcontroller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/dashboard',[Testlara::class, 'dashboard'])->middleware('auth');
Route::get('/product',[Testlara::class, 'productform'])->middleware('auth');
Route::post('/productins',[Testlara::class,'productins'])->middleware('auth');
Route::get('/viewproduct',[Testlara::class,'viewpro'])->middleware('auth');
Route::get('/product-delete/{id}',[Testlara::class,'productdel'])->middleware('auth');
Route::get('/product-edit/{id}',[Testlara::class,'productedit'])->middleware('auth');
Route::post('/editproductins',[Testlara::class,'producteditins'])->middleware('auth');
Route::get('/employee-form',[Admincon::class,'empform'])->middleware('auth');
Route::post('/addemployee',[Admincon::class,'addemployee'])->middleware('auth');
Route::get('/viewemployee',[Emplara::class,'viewemployee'])->middleware('auth');
Route::get('/customer',[Custcontroller::class,'addcustomer'])->middleware('auth');
Route::post('/customerins',[Custcontroller::class,'customerins'])->middleware('auth');
Route::get('/viewcustomer',[Custcontroller::class,'viewcustomer'])->middleware('auth');
Route::get('/addbill/{id}',[Billcontroller::class,'addbill'])->middleware('auth');
Route::post('/addbillins',[Billcontroller::class,'addbillins'])->middleware('auth');
Route::get('/viewbillproduct',[Billcontroller::class,'viewbillproduct'])->middleware('auth');
Route::get('/deletebillproduct/{id}',[Billcontroller::class,'deletebillproduct'])->middleware('auth');
Route::post('/confirmOrder',[Billcontroller::class,'confirmOrder'])->middleware('auth');
Route::get('/employee-user',[Emplara::class,'employee'])->middleware('auth');
Route::get('/admin-user',[Admincon::class,'admin'])->middleware('auth');
Route::get('/role',[Logincon::class,'role'])->middleware('auth');
Route::get('/admin-login',[Logincon::class,'adminlogin']);
Route::post('/login-checked',[Logincon::class,'logincheck']);



require __DIR__.'/auth.php';